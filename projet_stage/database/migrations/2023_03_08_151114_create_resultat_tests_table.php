<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('resultat_tests', function (Blueprint $table) {
            $table->id('idResultatTest');
            $table->integer('scoreTest');
            $table->date('dateTest');
            $table->unsignedBigInteger('hasEntrepriseId');
            $table->unsignedBigInteger('utilisateurId');
            $table->foreign('utilisateurId')->references('idUtilisateur')->on('utilisateur');
            $table->unsignedBigInteger('hasSecteurId');
            $table->foreign('hasSecteurId')->references('secteurId')->on('entreprise_has_secteurs');
            $table->foreign('hasEntrepriseId')->references('entrepriseId')->on('entreprise_has_secteurs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('resultat_tests');
    }
};
